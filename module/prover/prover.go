/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package prover

import (
	"chainmaker.org/chainmaker-cross/event"
	"chainmaker.org/chainmaker-cross/prover/impl"
)

// Prover is the interface to prove proof
type Prover interface {

	// GetType return the type of prover
	GetType() impl.ProverType

	// GetChainIDs return all the chain ids
	GetChainIDs() []string

	// ToProof convert to Proof for the inputs
	ToProof(chainID, txKey string, blockHeight int64, index int, contract *event.ContractInfo, extra []byte) (*event.Proof, error)

	// Prove load result of this proof
	Prove(proof *event.Proof) (bool, error)
}
