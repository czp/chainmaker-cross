/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package chainmaker

import (
	"errors"
	"fmt"
	"time"

	"chainmaker.org/chainmaker-cross/event"
	"chainmaker.org/chainmaker-cross/logger"
	"chainmaker.org/chainmaker-cross/prover"
	sdk "chainmaker.org/chainmaker-sdk-go"
	"chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"github.com/Rican7/retry"
	"github.com/Rican7/retry/strategy"
	"github.com/golang/protobuf/proto"
	"go.uber.org/zap"
)

const (
	WaitTimeOut     int64 = 15 // 单位：秒
	RetryCount            = 10
	RetryTimePeriod       = 2 * time.Second
)

// ChainMakerAdapter adapter of chainmaker
type ChainMakerAdapter struct {
	chainID    string                   // chainID
	dispatcher *prover.ProverDispatcher // chainmaker 交易证明的证明模块分发入口
	sdk        sdk.SDKInterface         // chainmaker sdk 实例
	logger     *zap.SugaredLogger       // 日志模块
}

// NewChainMakerAdapter create new instance of chainmaker adapter
func NewChainMakerAdapter(chainID, configPath string, logger *zap.SugaredLogger) (*ChainMakerAdapter, error) {
	chainMakerSdk, err := sdk.NewChainClient(
		sdk.WithConfPath(configPath),
	)
	if err != nil {
		return nil, err
	}
	return &ChainMakerAdapter{
		chainID:    chainID,
		dispatcher: prover.GetProverDispatcher(),
		sdk:        chainMakerSdk,
		logger:     logger,
	}, nil
}

// GetChainID return chain id
func (c *ChainMakerAdapter) GetChainID() string {
	return c.chainID
}

// Invoke transfer transaction-event which include the check of transaction prove
func (c *ChainMakerAdapter) Invoke(txEvent *event.TransactionEvent) (*event.TxResponse, error) {
	if txEvent.TxProof != nil {
		c.logger.Infof("cross[%s]->chain[%s]'s tx-proof need be prove", txEvent.CrossID, txEvent.ChainID)
		// 表示需要进行证明
		proveResult, err := c.dispatcher.Prove(txEvent.TxProof)
		if err != nil {
			c.logger.Errorf("cross[%s]->chain[%s]'s tx-proof prove failed, ", txEvent.CrossID, txEvent.ChainID, err)
			return nil, err
		}
		if !proveResult {
			c.logger.Errorf("cross[%s]->chain[%s]'s tx-proof prove failed, ", txEvent.CrossID, txEvent.ChainID, err)
			return nil, errors.New("this proof is wrong")
		}
		c.logger.Infof("cross[%s]->chain[%s]'s tx-proof prove success", txEvent.CrossID, txEvent.ChainID)
	} else {
		c.logger.Infof("cross[%s]->chain[%s]'s tx-proof not need be prove", txEvent.CrossID, txEvent.ChainID)
	}
	// 直接调用sdk处理该交易
	return c.invoke(txEvent)
}

// QueryByTxKey query transaction response by txkey
func (c *ChainMakerAdapter) QueryByTxKey(txKey string) (*event.CommonTxResponse, error) {
	if len(txKey) == 0 {
		return nil, fmt.Errorf("TxKey is <nil>")
	}
	transactionInfo, err := c.sdk.GetTxByTxId(txKey)
	if err != nil {
		return nil, err
	}
	// 将TransactionInfo转换为TxResponse
	return convertToTxResponse(transactionInfo), nil
}

// QueryTx query transaction and return response
func (c *ChainMakerAdapter) QueryTx(payload []byte) (*event.CommonTxResponse, error) {
	txRequest := &common.TxRequest{}
	if err := proto.Unmarshal(payload, txRequest); err != nil {
		return nil, fmt.Errorf("unmarshal transaction payload failed, %s", err.Error())
	}
	txKey := txRequest.Header.TxId
	c.logger.Infof("unmarshal find txKey = [%s]", txKey)
	return c.QueryByTxKey(txKey)
}

// invoke transfer transaction event and return response
func (c *ChainMakerAdapter) invoke(txEvent *event.TransactionEvent) (*event.TxResponse, error) {
	payload := txEvent.GetPayload()
	// 将该payload转换成为ChainMaker的请求
	txRequest := &common.TxRequest{}
	if err := proto.Unmarshal(payload, txRequest); err != nil {
		return nil, fmt.Errorf("unmarshal transaction payload failed, %s", err.Error())
	}
	c.logger.Infof("cross[%s]->chain[%s]'s tx-request unmarshalled", txEvent.CrossID, txEvent.ChainID)
	txResponse, err := c.sdk.SendTxRequest(txRequest, WaitTimeOut, false)
	if err != nil {
		c.logger.Errorf("cross[%s]->chain[%s]'s tx-request send failed, ", txEvent.CrossID, txEvent.ChainID, err)
		return nil, err
	}
	if txResponse.ContractResult == nil {
		c.logger.Errorf("cross[%s]->chain[%s]'s tx-request get empty response, ", txEvent.CrossID, txEvent.ChainID, err)
		return nil, err
	}
	txId, chainID, crossID := txRequest.Header.TxId, txEvent.ChainID, txEvent.CrossID
	c.logger.Infof("start get cross[%s]->chain[%s]'s tx-request[%s]'s result", crossID, chainID, txId)
	var txInfo *common.TransactionInfo
	err = retry.Retry(func(uint) error {
		c.logger.Infof("cross[%s]->chain[%s]'s tx[%s] get......", crossID, chainID, txId)
		txInfo, err = c.sdk.GetTxByTxId(txId)
		if err != nil {
			return err
		}
		return nil
	},
		strategy.Limit(RetryCount),
		strategy.Wait(RetryTimePeriod), // 指定超时等待
	)
	if err != nil {
		c.logger.Errorf("cross[%s]->chain[%s]'s tx[%s] load failed, ", crossID, chainID, txId, err)
		return nil, fmt.Errorf("cross[%s]->chain[%s]'s tx[%s] load failed, %s", crossID, chainID, txId, err.Error())
	}
	if txInfo == nil || txInfo.Transaction == nil || txInfo.Transaction.Result == nil {
		c.logger.Errorf("cross[%s]->chain[%s]'s tx[%s] load failed, ", crossID, chainID, txId, err)
		return nil, fmt.Errorf("cross[%s]->chain[%s]'s tx[%s] load failed", crossID, chainID, txId)
	}

	if txInfo.Transaction.Result.Code != common.TxStatusCode_SUCCESS {
		c.logger.Errorf("cross[%s]->chain[%s]'s tx[%s] invoke failed, result code = ", crossID, chainID, txId,
			txInfo.Transaction.Result.Code)
		return nil, errors.New(txInfo.Transaction.GetResult().ContractResult.Message)
	}
	// 交易失败或合约执行失败
	if txInfo.Transaction.GetResult().ContractResult.Code != common.ContractResultCode_OK {
		c.logger.Errorf("cross[%s]->chain[%s]'s tx[%s] invoke failed, contract result code = ", crossID, chainID, txId,
			txInfo.Transaction.GetResult().ContractResult.Code)
		return nil, errors.New(txInfo.Transaction.GetResult().ContractResult.Message)
	}
	contract, err := convertToContract(txInfo)
	if err != nil {
		c.logger.Errorf("cross[%s]->chain[%s]'s tx[%s] convert to contract failed, ", crossID, chainID, txId, err)
		return nil, fmt.Errorf("transaction[%s]'s type is not invoke user transaction, %s", txId, err.Error())
	}
	c.logger.Infof("cross[%s]->chain[%s]'s tx[%s] invoke success", crossID, chainID, txId)
	// 操作成功，则封装TxResponse
	txResp := event.NewTxResponse(chainID, txId, int64(txInfo.GetBlockHeight()), -1, contract, nil)
	return txResp, nil
}

// convertToContract convert transactionInfo to ContractInfo
func convertToContract(info *common.TransactionInfo) (*event.ContractInfo, error) {
	txType := info.GetTransaction().GetHeader().GetTxType()
	if txType == common.TxType_INVOKE_USER_CONTRACT {
		var transactPayload common.QueryPayload
		err := proto.Unmarshal(info.GetTransaction().GetRequestPayload(), &transactPayload)
		if err != nil {
			return nil, err
		}
		// 不关心version
		contract := event.NewContract(transactPayload.ContractName, "", transactPayload.Method, nil)
		txParams := transactPayload.Parameters
		for _, param := range txParams {
			innerParam := param
			contract.AddParameter(event.NewContractParameter(innerParam.Key, innerParam.Value))
		}
		return contract, nil
	} else {
		return nil, errors.New("transaction is not invoke user contract")
	}
}

// convertToTxResponse
func convertToTxResponse(info *common.TransactionInfo) *event.CommonTxResponse {
	contractInfo, _ := convertToContract(info)
	txResponse := event.NewTxResponse(info.Transaction.Header.ChainId, info.Transaction.Header.TxId, int64(info.BlockHeight), -1, contractInfo, nil)
	if info.Transaction.Result.Code == common.TxStatusCode_SUCCESS {
		return event.NewCommonTxResponse(txResponse, event.SuccessResp, "")
	}
	return event.NewCommonTxResponse(txResponse, event.FailureResp, info.Transaction.Result.ContractResult.Message)
}

func MockNilChainMakerAdapter() *ChainMakerAdapter {
	log := logger.GetLogger(logger.ModuleAdapter)
	pd := prover.GetProverDispatcher()
	return &ChainMakerAdapter{
		chainID:    "chainID",
		dispatcher: pd,
		sdk:        nil,
		logger:     log,
	}
}
