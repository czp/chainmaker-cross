/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package adapter

import (
	"fmt"
	"sync"

	"chainmaker.org/chainmaker-cross/event"
	"go.uber.org/zap"
)

var dispatcher *ChainAdapterDispatcher

func init() {
	dispatcher = &ChainAdapterDispatcher{
		adapters: make(map[string]ChainAdapter),
	}
}

// GetChainAdapterDispatcher return instance of ChainAdapterDispatcher which from config file
func GetChainAdapterDispatcher() *ChainAdapterDispatcher {
	return dispatcher
}

// ChainAdapterDispatcher dispatcher of chain adapter which can communication with real chain
type ChainAdapterDispatcher struct {
	sync.RWMutex                         // 读写锁
	adapters     map[string]ChainAdapter // 转接器的Map，按链ID区分
	log          *zap.SugaredLogger      // 日志模块
}

// SetLog set module of logger
func (d *ChainAdapterDispatcher) SetLog(log *zap.SugaredLogger) {
	d.log = log
}

// Register register adapter to adapter dispatcher
func (d *ChainAdapterDispatcher) Register(chainAdapter ChainAdapter) {
	chainID := chainAdapter.GetChainID()
	d.log.Infof("register adapter for chain[%s]", chainID)
	// 无需处理并发
	d.adapters[chainID] = chainAdapter
}

// Invoke transfer transaction event to real adapter
func (d *ChainAdapterDispatcher) Invoke(chainID string, tx *event.TransactionEvent) (*event.TxResponse, error) {
	d.RLock()
	defer d.RUnlock()
	if adapter, exist := d.adapters[chainID]; exist {
		d.log.Infof("find chain[%s]'s adapter", chainID)
		return adapter.Invoke(tx)
	}
	d.log.Errorf("can not find adapter for chain[%s]", chainID)
	return nil, fmt.Errorf("can not find adapter for chain[%v]", chainID)
}

// QueryByTxKey query transaction by chain-id and tx-key
func (d *ChainAdapterDispatcher) QueryByTxKey(chainID string, txKey string) (*event.CommonTxResponse, error) {
	d.RLock()
	defer d.RUnlock()
	if adapter, exist := d.adapters[chainID]; exist {
		d.log.Infof("find chain[%s]'s adapter", chainID)
		return adapter.QueryByTxKey(txKey)
	}
	d.log.Errorf("can not find adapter for chain[%s]", chainID)
	return nil, fmt.Errorf("can not find adapter for chain[%v]", chainID)
}

// Query query transaction by chain-id and payload
func (d *ChainAdapterDispatcher) Query(chainID string, payload []byte) (*event.CommonTxResponse, error) {
	d.RLock()
	defer d.RUnlock()
	if adapter, exist := d.adapters[chainID]; exist {
		d.log.Infof("find chain[%s]'s adapter", chainID)
		return adapter.QueryTx(payload)
	}
	d.log.Errorf("can not find adapter for chain[%s]", chainID)
	return nil, fmt.Errorf("can not find adapter for chain[%v]", chainID)
}

// GetChainIDs return all chain-ids which support by all adapters
func (d *ChainAdapterDispatcher) GetChainIDs() []string {
	chainIDs := make([]string, 0)
	for adapterKey := range d.adapters {
		chainID := adapterKey
		chainIDs = append(chainIDs, chainID)
	}
	return chainIDs
}
