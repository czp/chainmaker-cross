module chainmaker.org/chainmaker-cross/net

go 1.15

require (
	chainmaker.org/chainmaker-cross/logger v0.0.0
	github.com/Rican7/retry v0.1.0
	github.com/btcsuite/btcd v0.21.0-beta
	github.com/google/uuid v1.1.2
	github.com/libp2p/go-libp2p v0.13.0
	github.com/libp2p/go-libp2p-core v0.8.0
	github.com/multiformats/go-multiaddr v0.3.1
	github.com/stretchr/testify v1.6.1
	go.uber.org/zap v1.16.0
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

replace (
	chainmaker.org/chainmaker-cross/logger => ../logger
)
