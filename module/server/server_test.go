/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package server

import (
	"fmt"
	"testing"
	"time"

	"chainmaker.org/chainmaker-cross/event"
	"chainmaker.org/chainmaker-cross/utils"
)

const (
	chain1 = "chain1"
	chain2 = "chain2"
)

func TestServer_Start(t *testing.T) {
	//ymlFile, _ := filepath.Abs("../../config/cross_chain.yml")
	//localConf, err := conf.InitLocalConfigByFilepath(ymlFile)
	//conf.Config = localConf
	//if err != nil {
	//	t.Error(err)
	//}
	//logger.InitLogConfig(localConf.LogConfig)
	//server := NewServer()
	//transactionMgr := server.GetTransactionMgr()
	//_ = transactionMgr.GetEventChan()
	////eventChan := transactionMgr.GetEventChan()
	//err = server.Start()
	//if err != nil {
	//	t.Error(err)
	//}
	//t.Log("server start success!")
	////sendCrossEventTicker(10, eventChan)
	//// 等待关闭
	//ch := make(chan os.Signal, 1)
	//signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	//<-ch
	//t.Log("Received signal, shutting down...")
	//err = server.Stop()
	//if err != nil {
	//	t.Error(err)
	//}
}

func sendCrossEventTicker(count int, eventCh chan event.Event) {
	go func(count int, eventCh chan event.Event) {
		// 定时来处理
		for i := 0; i < count; i++ {
			fmt.Println("I will create new cross event and send it")
			crossEvent := innerNewCrossEvent()
			eventCh <- crossEvent
			time.Sleep(60 * time.Second)
		}
	}(count, eventCh)
}

func innerNewCrossEvent() *event.CrossEvent {
	crossTx0 := event.NewCrossTx(chain1, 0, []byte("execute"), []byte("commit"), []byte("rollback"))
	crossTx1 := event.NewCrossTx(chain2, 1, []byte("execute"), []byte("commit"), []byte("rollback"))
	crossTxs := make([]*event.CrossTx, 0)
	crossTxs = append(crossTxs, crossTx0, crossTx1)

	crossEvent := &event.CrossEvent{
		CrossID:   utils.NewUUID(),
		TxEvents:  event.NewCrossTxs(crossTxs),
		Version:   "v1.0.0",
		Timestamp: time.Now().Unix(),
		Extra:     []byte("chainmaker"),
	}
	return crossEvent
}
