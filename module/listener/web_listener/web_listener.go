/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package web_listener

import (
	"context"
	"net/http"
	"time"

	"chainmaker.org/chainmaker-cross/conf"
	"chainmaker.org/chainmaker-cross/listener/web_listener/methods"
	"chainmaker.org/chainmaker-cross/logger"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// WebListener is listener of web for CrossSDK
type WebListener struct {
	server *http.Server       // http server
	logger *zap.SugaredLogger // log
}

// NewWebListener create new instance of web listener
func NewWebListener() *WebListener {
	// 启动Web服务(默认Debug级别)
	gin.SetMode(gin.ReleaseMode)
	// 生成route
	ginRouter := gin.Default()
	// 初始化路由配置
	initRouter(ginRouter)
	// new server
	srv := &http.Server{
		Addr:    conf.Config.ListenerConfig.WebConfig.ToUrl(),
		Handler: ginRouter,
	}

	return &WebListener{
		server: srv,
		logger: logger.GetLogger(logger.ModuleWebListener),
	}
}

// ListenStart web listener server start
func (wl *WebListener) ListenStart() error {
	// 设置logger
	methods.InitHandlers(wl.logger)
	// 启动Http服务
	go func() {
		// service connections
		if err := wl.server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			wl.logger.Error("Web Server Listen:", err)
		}
	}()
	return nil
}

func initRouter(router *gin.Engine) {
	group := router.Group("/")
	initControllers(group) // 定义接口
}

// initControllers 初始化Controller配置
func initControllers(routeGroup *gin.RouterGroup) {
	routeGroup.POST(methods.CrossTag, methods.Dispatch)
}

// Stop web listener server stop
func (wl *WebListener) Stop() error {
	// delay
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := wl.server.Shutdown(ctx); err != nil {
		wl.logger.Error("Web Server Shutdown:", err)
	}
	wl.logger.Info("Module web-listener stopped")
	return nil
}
