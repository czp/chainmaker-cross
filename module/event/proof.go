/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package event

// Proofer interface of proof
type Proofer interface {

	// GetChainID return chain-id
	GetChainID() string

	// GetTxKey return the key of proof
	GetTxKey() string

	// GetBlockHeight return the height of proof
	GetBlockHeight() int64

	// GetIndex return index of tx
	GetIndex() int

	// GetContract return contract info
	GetContract() *ContractInfo

	// GetExtra return extra data
	GetExtra() []byte
}

// Proof the struct of proof
type Proof struct {
	ChainID     string        // chainID
	TxKey       string        // Tx数据的唯一键
	BlockHeight int64         // 块高
	Index       int           // 交易 index
	Contract    *ContractInfo // 合约调用数据
	Extra       []byte        // 未定义字段
}

// NewProof create new proof
func NewProof(chainID, txKey string, blockHeight int64, index int, contract *ContractInfo, extra []byte) *Proof {
	return &Proof{
		ChainID:     chainID,
		TxKey:       txKey,
		BlockHeight: blockHeight,
		Index:       index,
		Contract:    contract,
		Extra:       extra,
	}
}

// GetType return type of this event
func (p *Proof) GetType() EventType {
	return TxProofType
}

// GetChainID return chain-id
func (p *Proof) GetChainID() string {
	return p.ChainID
}

// GetTxKey return the key of tx
func (p *Proof) GetTxKey() string {
	return p.TxKey
}

// GetBlockHeight return block height
func (p *Proof) GetBlockHeight() int64 {
	return p.BlockHeight
}

// GetIndex return index in the block
func (p *Proof) GetIndex() int {
	return p.Index
}

// GetContract return contract info
func (p *Proof) GetContract() *ContractInfo {
	return p.Contract
}

// GetExtra return extra data
func (p *Proof) GetExtra() []byte {
	return p.Extra
}
