/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package event

import (
	"time"

	"chainmaker.org/chainmaker-cross/utils"
)

const (
	DefaultVersion = "v0.9.0"
)

// CrossEvent cross event
type CrossEvent struct {
	CrossID   string    // 跨链消息的唯一ID
	TxEvents  *CrossTxs // 跨链消息中包含的平行链交易体数据
	Version   string    // 版本号
	Timestamp int64     // 时间戳
	Extra     []byte    // 其他信息，拓展字段
}

// NewCrossEvent create cross event by array of CrossTx
func NewCrossEvent(txEvents []*CrossTx) *CrossEvent {
	crossID := utils.NewUUID()
	return &CrossEvent{
		CrossID:   crossID,
		Version:   DefaultVersion,
		Timestamp: time.Now().Unix(),
		TxEvents:  NewCrossTxs(txEvents),
	}
}

// NewEmptyCrossEvent create empty cross event which have not any CrossTx
func NewEmptyCrossEvent() *CrossEvent {
	crossID := utils.NewUUID()
	return &CrossEvent{
		CrossID:   crossID,
		Version:   DefaultVersion,
		Timestamp: time.Now().Unix(),
		TxEvents:  &CrossTxs{},
	}
}

// SetExtra set extra
func (c *CrossEvent) SetExtra(extra []byte) {
	c.Extra = extra
}

// SetVersion set version
func (c *CrossEvent) SetVersion(version string) {
	c.Version = version
}

// SetTimestamp set timestamp
func (c *CrossEvent) SetTimestamp(timestamp int64) {
	c.Timestamp = timestamp
}

// SetCrossID set cross-id
func (c *CrossEvent) SetCrossID(crossID string) {
	c.CrossID = crossID
}

// GetType return type of this event
func (c *CrossEvent) GetType() EventType {
	return CrossEventType
}

// GetPkgTxEvents return set of tx-event
func (c *CrossEvent) GetPkgTxEvents() *CrossTxs {
	return c.TxEvents
}

// IsValid check txs
func (c *CrossEvent) IsValid() bool {
	for i, tx := range c.TxEvents.GetCrossTxs() {
		if tx.Index != i {
			return false
		}
	}
	return true
}

// GetChainIDs return array of chain-id
func (c *CrossEvent) GetChainIDs() []string {
	chainIDs := make([]string, 0)
	if c.TxEvents != nil {
		for _, crossTx := range c.TxEvents.GetCrossTxs() {
			innerCrossTx := crossTx
			chainIDs = append(chainIDs, innerCrossTx.GetChainID())
		}
	}
	return chainIDs
}

// return cross-id
func (c *CrossEvent) GetCrossID() string {
	return c.CrossID
}

// CrossSearchEvent cross search struct
type CrossSearchEvent struct {
	CrossID string
}

// NewCrossSearchEvent create cross search event
func NewCrossSearchEvent(crossID string) *CrossSearchEvent {
	return &CrossSearchEvent{
		CrossID: crossID,
	}
}

// GetType return type of this event
func (c *CrossSearchEvent) GetType() EventType {
	return CrossEventSearchType
}

// return cross-id
func (c *CrossSearchEvent) GetCrossID() string {
	return c.CrossID
}
