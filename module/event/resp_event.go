/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package event

import (
	"sync"
	"time"
)

const (
	SuccessResp = iota
	FailureResp
	ErrorResp
	UnknownResp
)

// CrossResponse the struct of cross response
type CrossResponse struct {
	CrossID     string
	Code        int32
	Msg         string
	TxResponses []*CrossTxResponse
}

// NewCrossResponse create new cross response object
func NewCrossResponse(crossID string, code int32, msg string) *CrossResponse {
	return &CrossResponse{
		CrossID:     crossID,
		Code:        code,
		Msg:         msg,
		TxResponses: make([]*CrossTxResponse, 0),
	}
}

// DefaultCrossResponse return new cross response which include empty cross tx response
func DefaultCrossResponse() *CrossResponse {
	return &CrossResponse{
		TxResponses: make([]*CrossTxResponse, 0),
	}
}

// AddTxResponse add tx response
func (c *CrossResponse) AddTxResponse(response *CrossTxResponse) {
	c.TxResponses = append(c.TxResponses, response)
}

// GetType return type of this event
func (c *CrossResponse) GetType() EventType {
	return CrossRespEventType
}

// CrossTxResponse the struct of cross tx response
type CrossTxResponse struct {
	ChainID     string // chainID
	TxKey       string // 交易体的uuid
	BlockHeight int64  // 交易执行的块高
	Index       int    // 执行顺序
	Extra       []byte // 拓展字段
}

// NewCrossTxResponse create new tx response
func NewCrossTxResponse(chainID, txKey string, blockHeight int64, index int, extra []byte) *CrossTxResponse {
	return &CrossTxResponse{
		ChainID:     chainID,
		TxKey:       txKey,
		BlockHeight: blockHeight,
		Index:       index,
		Extra:       extra,
	}
}

// TxResponse the struct of tx response
type TxResponse struct {
	ChainID     string        // chainID
	TxKey       string        // tx的uuid
	BlockHeight int64         // 交易执行的块高，提供给 prover 进行验证
	Index       int           // 执行顺序
	Contract    *ContractInfo // 合约调用数据
	Extra       []byte        // 拓展字段
}

// NewTxResponse create new tx response
func NewTxResponse(chainID, txKey string, blockHeight int64, index int, contract *ContractInfo, extra []byte) *TxResponse {
	return &TxResponse{
		ChainID:     chainID,
		TxKey:       txKey,
		BlockHeight: blockHeight,
		Index:       index,
		Contract:    contract,
		Extra:       extra,
	}
}

// CommonTxResponse the struct of common tx response
type CommonTxResponse struct {
	TxResponse
	Code int32
	Msg  string
}

// NewCommonTxResponse create new common tx response
func NewCommonTxResponse(txResponse *TxResponse, code int32, msg string) *CommonTxResponse {
	resp := &CommonTxResponse{
		Code: code,
		Msg:  msg,
	}
	resp.TxResponse = *txResponse
	return resp
}

// IsSuccess return whether is success
func (c *CommonTxResponse) IsSuccess() bool {
	return c.Code == SuccessResp
}

// ContractInfo the struct of contarct info
type ContractInfo struct {
	Name       string               // 合约名
	Version    string               // 合约版本
	Method     string               // 合约方法
	Parameters []*ContractParameter // 合约参数
	ExtraData  []byte               // 拓展字段
}

// NewContract create new contract
func NewContract(name, version, method string, extraData []byte) *ContractInfo {
	return &ContractInfo{
		Name:       name,
		Version:    version,
		Method:     method,
		Parameters: make([]*ContractParameter, 0),
		ExtraData:  extraData,
	}
}

// AddParameter add parameter to contractInfo
func (c *ContractInfo) AddParameter(parameter *ContractParameter) {
	c.Parameters = append(c.Parameters, parameter)
}

// AddParameters add set of parameter to contractInfo
func (c *ContractInfo) AddParameters(parameter []*ContractParameter) {
	c.Parameters = append(c.Parameters, parameter...)
}

// ContractParameter the struct of contract parameter
type ContractParameter struct {
	Key   string // 调用参数的key
	Value string // 调用参数的value
}

// NewContractParameter create new contract parameter
func NewContractParameter(key, value string) *ContractParameter {
	return &ContractParameter{
		Key:   key,
		Value: value,
	}
}

// NewContractValue create new ContractParameter
func NewContractValue(value string) *ContractParameter {
	return NewContractParameter("", value)
}

// ProofResponse the struct of proof response
type ProofResponse struct {
	sync.Mutex             // lock
	CrossID     string     // 跨链uuid
	OpFunc      OpFuncType // 操作类型
	Code        int        // 状态码
	Msg         string     // 错误消息
	Key         string     // 当前对象的uuid
	TxResponse             // adapter交互的应答
	ch          chan bool  // 同步/异步通道
	isCompleted bool       // 是否完成的标签
}

// NewProofResponse create new proof response
func NewProofResponse(crossID, chainID string, opFunc OpFuncType) *ProofResponse {
	pr := &ProofResponse{
		CrossID: crossID,
		OpFunc:  opFunc,
		Code:    UnknownResp,
		ch:      make(chan bool, 1),
	}
	pr.ChainID = chainID
	return pr
}

// NewProofResponseByProof create new proof response by proof
func NewProofResponseByProof(crossID, chainID, msg string, code int, opFunc OpFuncType, proof *Proof) *ProofResponse {
	proofResp := NewProofResponse(crossID, chainID, opFunc)
	proofResp.TxResponse = *NewTxResponse(proof.ChainID, proof.TxKey, proof.BlockHeight, proof.Index, proof.Contract, proof.Extra)
	proofResp.Msg = msg
	proofResp.Code = code
	return proofResp
}

// SetKey set key of proof response
func (p *ProofResponse) SetKey(key string) {
	p.Key = key
}

// GetKey return key of proof response
func (p *ProofResponse) GetKey() string {
	return p.Key
}

// GetType return type of this event
func (p *ProofResponse) GetType() EventType {
	return ProofRespEventType
}

// IsSuccess return whether is success
func (p *ProofResponse) IsSuccess() bool {
	return p.Code == SuccessResp
}

// DoneError update state to failed
func (p *ProofResponse) DoneError(msg string) {
	p.Lock()
	defer p.Unlock()
	if !p.isCompleted {
		p.Code, p.Msg = FailureResp, msg
		p.isCompleted = true
		p.ch <- true
	}
}

// Done update state and set tx-info
func (p *ProofResponse) Done(chainID, txKey string, blockHeight int64, index int, contract *ContractInfo, extra []byte) {
	p.Lock()
	defer p.Unlock()
	if !p.isCompleted {
		p.Code = SuccessResp
		p.ChainID, p.TxKey = chainID, txKey
		p.BlockHeight, p.Index = blockHeight, index
		p.Contract, p.Extra = contract, extra
		p.isCompleted = true
		p.ch <- true
	}
}

// Wait wait until {waitTime} or completed
func (p *ProofResponse) Wait(waitTime time.Duration) bool {
	// 等待ch处理完成
	select {
	case <-time.After(waitTime):
		log.Warnf("waiting response timeout for [%s]", p.CrossID)
	case <-p.ch:
		log.Infof("waiting response success for [%s]", p.CrossID)
	}
	return p.isCompleted
}
