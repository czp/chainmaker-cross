/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package sdk

import (
	"fmt"

	"chainmaker.org/chainmaker-cross/sdk/parallels"
	"chainmaker.org/chainmaker-cross/sdk/parallels/chainmaker"
)

// ClientFactory return ClientInterface by chain type and chain id
func ClientFactory(chainType parallels.ChainType, chainID string, confPath string) (parallels.ClientInterface, error) {
	switch chainType {
	case parallels.ChainChainMaker:
		return chainmaker.NewClient(chainID, confPath)
	default:
		return nil, fmt.Errorf("chain type unknown")
	}
}
