/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"fmt"

	"chainmaker.org/chainmaker-cross/sdk"
	conf "chainmaker.org/chainmaker-cross/sdk/config"
	"chainmaker.org/chainmaker-cross/sdk/parallels"
	"chainmaker.org/chainmaker-go/common/json"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

func main() {
	mainCmd := &cobra.Command{Use: "cross-chain-cli"}
	mainCmd.AddCommand(DeliverEventCMD())
	mainCmd.AddCommand(ShowCrossResultCMD())

	err := mainCmd.Execute()
	if err != nil {
		_ = fmt.Errorf("cross-chain-cli error, %v", err)
	}
	return
}

// DeliverEventCMD deliver event command
func DeliverEventCMD() *cobra.Command {
	deliverCmd := &cobra.Command{
		Use:   "deliver",
		Short: "Deliver CrossEvent",
		Long:  "Deliver CrossEvent To Proxy",
		RunE: func(cmd *cobra.Command, _ []string) error {
			cmViper := viper.New()
			paramPath, err := cmd.Flags().GetString(flagNameOfParams)
			if err != nil {
				return fmt.Errorf("missing flag --params")
			}
			cmViper.SetConfigFile(paramPath)
			if err := cmViper.ReadInConfig(); err != nil {
				return fmt.Errorf("read params error: %v", err)
			}
			crossParams := &CrossTxParams{}
			if err := cmViper.Unmarshal(crossParams); err != nil {
				return fmt.Errorf("unmarshal params error: %v", err)
			}
			// 创建 CrossEvent
			uitpEvent := sdk.NewUITPCrossEvent()

			// 获取本地跨链sdk配置
			configM, _ := conf.InitConfigByFilepath(ConfigFilepath)

			for _, crossParam := range crossParams.Params {
				// 获取平行链的 MultiTXBuilder
				mtxBuilder, err := sdk.MultiTxFactory(parallels.ChainType(crossParam.ChainType), crossParam.ChainID, configM)
				if err != nil {
					return fmt.Errorf("get chain:[%s] txbuilder error", err)
				}
				// 生成 CrossTx
				crossTx, err := mtxBuilder.GetClient().BuildTxRequestData(
					uitpEvent.GetCrossID(),
					crossParam.ContractName,
					crossParam.ExecuteMethod, crossParam.ExecuteParams,
					crossParam.RollbackMethod, crossParam.RollbackParams,
					crossParam.Index,
				)
				if err != nil {
					return fmt.Errorf("build tx request error:[%s]", err.Error())
				}
				// 组装 CrossTx
				err = uitpEvent.WithMultiTxs(crossTx)
				if err != nil {
					fmt.Print(err)
					continue
				}
			}
			// 提交到 CrossChain Proxy 节点
			resp, err := uitpEvent.DeliverEvent(DefaultURL)
			if err != nil {
				return fmt.Errorf("deliver tx error:[%s]", err.Error())
			}
			if resp.CrossID == "" {
				return fmt.Errorf("deliver tx error, remote server may stoped")
			}
			respStr, err := json.Marshal(resp)
			if err != nil {
				return nil
			}
			fmt.Printf("%s", respStr)
			return nil
		},
	}
	attachFlags(deliverCmd, []string{flagNameOfConfigFilepath, flagNameOfParams, flagNameOfUrl})
	deliverCmd.Flags().String(flagNameOfParams, "", "the parameters for cross tx")
	return deliverCmd
}

// ShowCrossResultCMD show cross result command
func ShowCrossResultCMD() *cobra.Command {
	showCmd := &cobra.Command{
		Use:   "show",
		Short: "Show CrossEvent Result",
		Long:  "Show CrossEvent Result By Proxy",
		RunE: func(cmd *cobra.Command, _ []string) error {
			crossID, err := cmd.Flags().GetString(flagNameOfCrossID)
			if err != nil {
				return fmt.Errorf("missing flag --crossID")
			}
			// 使用 crossID 查询跨链执行结果
			resp, err := sdk.ShowCrossResult(DefaultURL, crossID)
			if err != nil {
				return fmt.Errorf("show cross result error: [%s]", err.Error())
			}
			respStr, err := json.Marshal(resp)
			if err != nil {
				return nil
			}
			fmt.Printf("%s", respStr)
			return nil
		},
	}
	attachFlags(showCmd, []string{flagNameOfCrossID, flagNameOfUrl})
	showCmd.Flags().String(flagNameOfCrossID, "", "the cross id for event")
	return showCmd
}

func initFlagSet() *pflag.FlagSet {
	flags := &pflag.FlagSet{}
	flags.StringVarP(&ConfigFilepath, flagNameOfConfigFilepath, flagNameShortHandOfConfigFilepath, ConfigFilepath, "specify config file path, if not set, default use ./cross_chain_sdk.yml")
	flags.StringVarP(&DefaultURL, flagNameOfUrl, flagNameShortHandOfUrl, DefaultURL, "specify default url, if not set, default use http://localhost:8080")
	return flags
}

func attachFlags(cmd *cobra.Command, flagNames []string) {
	flags := initFlagSet()
	cmdFlags := cmd.Flags()
	for _, flagName := range flagNames {
		if flag := flags.Lookup(flagName); flag != nil {
			cmdFlags.AddFlag(flag)
		}
	}
}
