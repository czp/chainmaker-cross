/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package chainmaker

import (
	"chainmaker.org/chainmaker-cross/event"
	"chainmaker.org/chainmaker-cross/sdk/parallels"
)

var _ parallels.MultiTx = (*CrossTxWrapper)(nil)

// CrossTxWrapper wrapper crossTx for parallels
type CrossTxWrapper struct {
	event *event.CrossTx
}

// ToCrossTx convert para-chain MultiTx to crossTx
func (c *CrossTxWrapper) ToCrossTx() *event.CrossTx {
	return c.event
}

// Sign CrossTx by local chain client private key
func (c *CrossTxWrapper) Sign() ([]byte, error) {
	panic("implement me")
}
