/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package chainmaker

import (
	"fmt"

	"chainmaker.org/chainmaker-cross/event"
	conf "chainmaker.org/chainmaker-cross/sdk/config"
	"chainmaker.org/chainmaker-cross/sdk/parallels"
	"chainmaker.org/chainmaker-go/common/serialize"
	"github.com/golang/protobuf/proto"
)

var _ parallels.MultiTxBuilder = (*Builder)(nil)

// Builder chainmaker implement instance of MultiTxBuilder
type Builder struct {
	ChainID         string
	Client          *ChainClient
	Config          *conf.CrossChainConf
	Params          map[parallels.TxRequestType]parallels.TxRequestParams
	ExecutePayload  []byte
	CommitPayload   []byte
	RollbackPayload []byte
}

// GetChainID return chain id
func (b *Builder) GetChainID() string {
	return b.ChainID
}

// GetClient return chain client
func (b *Builder) GetClient() parallels.ClientInterface {
	return b.Client
}

// UseParam load TxRequestParams
func (b *Builder) UseParam(params ...parallels.TxRequestParams) parallels.MultiTxBuilder {
	for _, param := range params {
		b.Params[param.GetType()] = param
	}
	return b
}

// WithOpts apply MultiTxOpt func
func (b *Builder) WithOpts(opts ...parallels.MultiTxOpt) parallels.MultiTxBuilder {
	for _, opt := range opts {
		opt(b)
	}
	return b
}

// Final create multiTx by tx builder
func (b *Builder) Final(chainID string, index int) parallels.MultiTx {
	return &CrossTxWrapper{
		event: &event.CrossTx{
			ChainID:         chainID,
			Index:           index,
			ExecutePayload:  b.ExecutePayload,
			CommitPayload:   b.CommitPayload,
			RollbackPayload: b.RollbackPayload,
		},
	}
}

func (b *Builder) buildExecutePayload(p *ExecuteCallRequest) ([]byte, error) {
	eParams := map[string]string{
		b.Config.BusinessCrossIDKey:      p.crossID,
		b.Config.BusinessContractNameKey: p.executeContractName,
		b.Config.BusinessMethodKey:       p.executeMethod,
		b.Config.BusinessParamsKey:       string(serialize.EasyMarshal(serialize.ParamsMapToEasyCodecItem(p.executeParams))),
	}
	rParams := map[string]string{
		b.Config.BusinessCrossIDKey:      p.crossID,
		b.Config.BusinessContractNameKey: p.rollbackContractName,
		b.Config.BusinessMethodKey:       p.rollbackMethod,
		b.Config.BusinessParamsKey:       string(serialize.EasyMarshal(serialize.ParamsMapToEasyCodecItem(p.rollbackParams))),
	}
	params := map[string]string{
		b.Config.BusinessCrossIDKey:         p.crossID,
		b.Config.TransactionExecuteDataKey:  string(serialize.EasyMarshal(serialize.ParamsMapToEasyCodecItem(eParams))),
		b.Config.TransactionRollbackDataKey: string(serialize.EasyMarshal(serialize.ParamsMapToEasyCodecItem(rParams))),
	}
	request, err := b.Client.GetTxRequest(
		b.Config.TransactionContractName,
		b.Config.TransactionExecuteMethod,
		"",
		params,
	)
	if err != nil {
		return nil, err
	}
	bz, err := proto.Marshal(request)
	if err != nil {
		return nil, err
	}
	return bz, nil
}

func (b *Builder) buildCommitPayload(p *CommitCallRequest) ([]byte, error) {
	params := map[string]string{
		b.Config.BusinessCrossIDKey: p.crossID,
	}
	request, err := b.Client.GetTxRequest(
		b.Config.TransactionContractName,
		b.Config.TransactionCommitMethod,
		"",
		params,
	)
	if err != nil {
		return nil, err
	}
	bz, err := proto.Marshal(request)
	if err != nil {
		return nil, err
	}
	return bz, nil
}

func (b *Builder) buildRollbackPayload(p *RollbackCallRequest) ([]byte, error) {
	params := map[string]string{
		b.Config.BusinessCrossIDKey: p.crossID,
	}
	request, err := b.Client.GetTxRequest(
		b.Config.TransactionContractName,
		b.Config.TransactionRollbackMethod,
		"",
		params,
	)
	if err != nil {
		return nil, err
	}
	bz, err := proto.Marshal(request)
	if err != nil {
		return nil, err
	}
	return bz, nil
}

type retData struct {
	ExecutePayload  []byte
	CommitPayload   []byte
	RollbackPayload []byte
	Error           error
}

func BuildExecuteMultiTxOpt(ret *retData, p *ExecuteCallRequest) parallels.MultiTxOpt {
	return func(b parallels.MultiTxBuilder) {
		if ret.Error != nil {
			return
		}
		builder, ok := b.(*Builder)
		if !ok {
			ret.Error = fmt.Errorf("MultiTxBuilder type assert fail ")
			return
		}
		ret.ExecutePayload, ret.Error = builder.buildExecutePayload(p)
	}
}

func BuildCommitMultiTxOpt(ret *retData, p *CommitCallRequest) parallels.MultiTxOpt {
	return func(b parallels.MultiTxBuilder) {
		if ret.Error != nil {
			return
		}
		builder, ok := b.(*Builder)
		if !ok {
			ret.Error = fmt.Errorf("MultiTxBuilder type assert fail ")
			return
		}
		ret.CommitPayload, ret.Error = builder.buildCommitPayload(p)
	}
}

func BuildRollbackMultiTxOpt(ret *retData, p *RollbackCallRequest) parallels.MultiTxOpt {
	return func(b parallels.MultiTxBuilder) {
		if ret.Error != nil {
			return
		}
		builder, ok := b.(*Builder)
		if !ok {
			ret.Error = fmt.Errorf("MultiTxBuilder type assert fail ")
			return
		}
		ret.RollbackPayload, ret.Error = builder.buildRollbackPayload(p)
	}
}

// BuildTxGetTxRequestExecute imply client method here
func BuildTxGetTxRequestExecute(b parallels.MultiTxBuilder) {
	builder, ok := b.(*Builder)
	if !ok {
		panic("err")
	}
	p, ok := builder.Params[parallels.TypeExecuteRequest].(*ExecuteCallRequest)
	if !ok {
		panic("params convert error")
	}
	eParams := map[string]string{
		builder.Config.BusinessCrossIDKey:      p.crossID,
		builder.Config.BusinessContractNameKey: p.executeContractName,
		builder.Config.BusinessMethodKey:       p.executeMethod,
		builder.Config.BusinessParamsKey:       string(serialize.EasyMarshal(serialize.ParamsMapToEasyCodecItem(p.executeParams))),
	}
	rParams := map[string]string{
		builder.Config.BusinessCrossIDKey:      p.crossID,
		builder.Config.BusinessContractNameKey: p.rollbackContractName,
		builder.Config.BusinessMethodKey:       p.rollbackMethod,
		builder.Config.BusinessParamsKey:       string(serialize.EasyMarshal(serialize.ParamsMapToEasyCodecItem(p.rollbackParams))),
	}
	params := map[string]string{
		builder.Config.BusinessCrossIDKey:         p.crossID,
		builder.Config.TransactionExecuteDataKey:  string(serialize.EasyMarshal(serialize.ParamsMapToEasyCodecItem(eParams))),
		builder.Config.TransactionRollbackDataKey: string(serialize.EasyMarshal(serialize.ParamsMapToEasyCodecItem(rParams))),
	}
	request, err := builder.Client.GetTxRequest(
		builder.Config.TransactionContractName,
		builder.Config.TransactionExecuteMethod,
		"",
		params,
	)
	if err != nil {
		panic(err)
	}
	bz, err := proto.Marshal(request)
	if err != nil {
		panic(err)
	}
	// 装载 executePayload 到 builder
	builder.ExecutePayload = bz
}

// BuildTxGetTxRequestCommit imply client method here
func BuildTxGetTxRequestCommit(b parallels.MultiTxBuilder) {
	builder, ok := b.(*Builder)
	if !ok {
		panic("err")
	}
	p, ok := builder.Params[parallels.TypeCommitRequest].(*CommitCallRequest)
	if !ok {
		panic("params convert error")
	}
	params := map[string]string{
		builder.Config.BusinessCrossIDKey: p.crossID,
	}
	request, err := builder.Client.GetTxRequest(
		builder.Config.TransactionContractName,
		builder.Config.TransactionCommitMethod,
		"",
		params,
	)
	if err != nil {
		panic(err)
	}
	bz, err := proto.Marshal(request)
	if err != nil {
		panic(err)
	}
	// 装载 CommitPayload 到 builder
	builder.CommitPayload = bz
}

// BuildTxGetTxRequestRollback imply client method here
func BuildTxGetTxRequestRollback(b parallels.MultiTxBuilder) {
	builder, ok := b.(*Builder)
	if !ok {
		panic("err")
	}
	p, ok := builder.Params[parallels.TypeRollbackRequest].(*RollbackCallRequest)
	if !ok {
		panic("params convert error")
	}
	params := map[string]string{
		builder.Config.BusinessCrossIDKey: p.crossID,
	}
	request, err := builder.Client.GetTxRequest(
		builder.Config.TransactionContractName,
		builder.Config.TransactionRollbackMethod,
		"",
		params,
	)
	if err != nil {
		panic(err)
	}
	bz, err := proto.Marshal(request)
	if err != nil {
		panic(err)
	}
	builder.RollbackPayload = bz
}
