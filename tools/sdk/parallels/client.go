/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package parallels

import (
	"chainmaker.org/chainmaker-cross/event"
)

// ClientInterface is interface for Cross Chain Client
type ClientInterface interface {

	// BuildTxRequestData create crossTx
	BuildTxRequestData(crossID string, contractName string, executeMethod string, executeParams map[string]string, rollbackMethod string, rollbackParams map[string]string, index int) (*event.CrossTx, error)

	//Stop() error
}

// MultiTx 入参接口
type TxRequestParams interface {

	// GetType return type of tx-request
	GetType() TxRequestType

	// GetChainType return type of chain
	GetChainType() ChainType
}
