#### 跨链SDK使用说明

跨链SDK是提供给业务方使用的SDK，通过该SDK，业务方用户可以构建跨链请求，发送至对应的跨链网关（主跨链网关）。

前置需求：

> 部署跨链代理，能访问代理节点的web服务
>
> 配置平行链的证书
>
> 在平行链上分别部署事物合约，业务合约

上述需求请参考跨链代理部署文档

以下是跨链SDK的使用方式：

> 调用 `SDK` 源码
```go
var err error
// 创建 UITPCrossEvent 对象
uitpEvent := ccsdk.NewUITPCrossEvent()
// 获取本地跨链sdk配置
configM, err := conf.InitConfigByFilepath("./config/template/cross_chain_sdk.yml")
require.NoError(t, err)
// 获取平行链 chain1 的 MultiTXBuilder，chain1是链的ID
mtxBuilder1, err := MultiTxFactory(parallels.ChainChainMaker, "chain1", configM)
require.NoError(t, err)
require.NotNil(t, mtxBuilder1)
// 生成 CrossTx
crossTx1, err := mtxBuilder1.GetClient().BuildTxRequestData(
    uitpEvent.GetCrossID(),                     //  固定的crossID
    "BalanceStable",                            //  业务合约名称
    "Minus", map[string]string{"number": "1"},  //  Execute执行函数名及其入参
    "Reset", map[string]string{},               //  Rollback执行函数名及其入参
    0,                                          //  当前操作在跨链中的序号
)

// 获取平行链 chain2 的 MultiTXBuilder，chain2 是链的ID
mtxBuilder2, err := MultiTxFactory(parallels.ChainChainMaker, "chain2", configM)
require.NoError(t, err)
require.NotNil(t, mtxBuilder2)
// 可以通过获取 client 实例，调用非 SDKInterface 提供的方法，提供平行链非通用接口的入口
crossTx2, err := mtxBuilder2.GetClient().(*chainmaker.ChainClient).BuildTxRequestData(
    uitpEvent.GetCrossID(),
    "BalanceStable",
    "Plus", map[string]string{"number": "10000"},
    "Reset", map[string]string{},
    1,
)

// 组装 CrossTx
uitpEvent.WithMultiTxs(crossTx1, crossTx2)
// 提交到 CrossProxy 节点，该路径为 CrossProxy 对应的 WebListener 的访问路径
resp, err := uitpEvent.DeliverEvent("http://localhost:8080")
require.NoError(t, err)
t.Log("crossID: ", resp.CrossID)
require.NotEmpty(t, resp.CrossID)
// 异步查询跨链结果
// 使用 event 对象查询跨链执行结果
result, err := uitpEvent.ShowCrossResult("http://localhost:8080")
require.NoError(t, err)
t.Log(result)
// 也可以直接使用 crossID 查询跨链执行结果，需要注意尽量延时访问，以便于跨链操作完成
_, _ = ShowCrossResult("http://localhost:8080", uitpEvent.GetCrossID())
```

> 使用命令行工具

```shell script
cd cmd/cli
go build -o cross-chain-sdk-cli

## Deliver a CrossEvent
cross-chain-sdk-cli deliver
-c
/PathToYourProject/chainmaker-cross-chain/tools/sdk/config/template/cross_chain_sdk.yml
-u
http://localhost:8080
--params
/PathToYourProject/chainmaker-cross-chain/tools/sdk/config/template/cross_chain_params.yml

## Query Cross Result
cross-chain-sdk-cli show
-u
http://localhost:8080
--crossID
"XXXXXXX"
```