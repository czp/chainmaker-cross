/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package sdk

import (
	"fmt"
	"sync"
	"testing"

	conf "chainmaker.org/chainmaker-cross/sdk/config"
	"chainmaker.org/chainmaker-cross/sdk/parallels"
	"chainmaker.org/chainmaker-cross/sdk/parallels/chainmaker"
	"chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"github.com/golang/protobuf/proto"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
)

func TestNewUITPCrossEvent(t *testing.T) {
	var err error
	// 创建 CrossEvent
	uitpEvent := NewUITPCrossEvent()

	//  获取本地跨链sdk配置
	configM, err := conf.InitConfigByFilepath("./config/template/cross_chain_sdk.yml")
	require.NoError(t, err)

	// 获取平行链的 MultiTXBuilder1
	mtxBuilder1, err := MultiTxFactory(parallels.ChainChainMaker, "chain1", configM)
	require.NoError(t, err)
	require.NotNil(t, mtxBuilder1)
	// 生成 CrossTx
	crossTx1, err := mtxBuilder1.GetClient().BuildTxRequestData(
		uitpEvent.GetCrossID(),
		"BalanceStable",
		"Minus", map[string]string{"number": "1"},
		"Reset", map[string]string{},
		0,
	)
	require.NoError(t, err)

	// 获取平行链的 MultiTXBuilder2
	mtxBuilder2, err := MultiTxFactory(parallels.ChainChainMaker, "chain2", configM)
	require.NoError(t, err)
	require.NotNil(t, mtxBuilder2)
	// 可以通过获取 client 实例，调用非 SDKInterface 提供的方法，提供平行链非通用接口的入口
	crossTx2, err := mtxBuilder2.GetClient().(*chainmaker.ChainClient).BuildTxRequestData(
		uitpEvent.GetCrossID(),
		"BalanceStable",
		"Plus", map[string]string{"number": "1"},
		"Reset", map[string]string{},
		1,
	)
	require.NoError(t, err)

	// 组装 CrossTx
	uitpEvent.WithMultiTxs(crossTx1, crossTx2)
	// 提交到 CrossChain Proxy 节点
	resp, _ := uitpEvent.DeliverEvent("http://localhost:8080")
	t.Log("crossID: ", resp.CrossID)
	require.NotEmpty(t, resp.CrossID)
	// 异步查询跨链结果
	// 使用 event 对象查询跨链执行结果
	result, err := uitpEvent.ShowCrossResult("http://localhost:8080")
	require.NoError(t, err)
	t.Log(result)
	// 使用 crossID 查询跨链执行结果
	_, _ = ShowCrossResult("http://localhost:8080", uitpEvent.GetCrossID())
}

func TestShowCrossResult(t *testing.T) {
	result, err := ShowCrossResult("http://localhost:8080", "651df829fb9b494087940a9a260696e1")
	require.NoError(t, err)
	t.Log(result)
}

func TestRepeatSendCrossEvent(t *testing.T) {
	//  获取本地跨链sdk配置
	configM, err := conf.InitConfigByFilepath("./config/template/cross_chain_sdk.yml")
	require.NoError(t, err)
	for {
		// 创建 CrossEvent
		uitpEvent := NewUITPCrossEvent()

		// 获取平行链的 MultiTXBuilder1
		mtxBuilder1, err := MultiTxFactory(parallels.ChainChainMaker, "chainID1", configM)
		require.NoError(t, err)
		require.NotNil(t, mtxBuilder1)
		// 生成 CrossTx
		crossTx1, err := mtxBuilder1.GetClient().BuildTxRequestData(
			uitpEvent.GetCrossID(),
			"BalanceStable",
			"Minus", map[string]string{"number": "1"},
			"Reset", map[string]string{},
			0)
		if err != nil {
			panic(err)
		}

		// 获取平行链的 MultiTXBuilder2
		mtxBuilder2, err := MultiTxFactory(parallels.ChainChainMaker, "chain2", configM)
		require.NoError(t, err)
		require.NotNil(t, mtxBuilder1)
		// 可以通过获取 client 实例，调用非 SDKInterface 提供的方法，提供平行链非通用接口的入口
		crossTx2, err := mtxBuilder2.GetClient().(*chainmaker.ChainClient).BuildTxRequestData(
			uitpEvent.GetCrossID(),
			"BalanceStable",
			"Plus", map[string]string{"number": "1"},
			"Reset", map[string]string{},
			1)
		if err != nil {
			panic(err)
		}

		// 组装 CrossTx
		uitpEvent.WithMultiTxs(crossTx1, crossTx2)
		// 提交到 CrossChain Proxy 节点
		resp, err := uitpEvent.DeliverEvent("http://localhost:8080")
		require.NoError(t, err)
		t.Log(resp)
	}
}

func Benchmark_BuildTxRequestData(b *testing.B) {
	//  获取本地跨链sdk配置
	configM, err := conf.InitConfigByFilepath("./config/template/cross_chain_sdk.yml")
	require.NoError(b, err)

	// 获取平行链的 MultiTXBuilder1
	mtxBuilder1, err := MultiTxFactory(parallels.ChainChainMaker, "chain1", configM)
	require.NoError(b, err)
	require.NotNil(b, mtxBuilder1)
	g := sync.WaitGroup{}
	g.Add(b.N)
	for i := 0; i < b.N; i++ {
		go func(index int) {
			defer g.Done()
			defer build(index, mtxBuilder1)
		}(i)
	}
	g.Wait()
}

func build(index int, builder parallels.MultiTxBuilder) {
	crossID := uuid.New().String()
	crossTx, _ := builder.GetClient().BuildTxRequestData(
		crossID,
		"BalanceStable",
		"Minus", map[string]string{"number": "1"},
		"Reset", map[string]string{},
		index,
	)
	request := &common.TxRequest{}
	proto.Unmarshal(crossTx.ExecutePayload, request)
	tp := &common.TransactPayload{}
	proto.Unmarshal(request.Payload, tp)
	find := false
	for _, kv := range tp.Parameters {
		if kv.Key == "crossID" {
			find = true
			if kv.Value != crossID {
				fmt.Println("crossTx.crossID ", kv.Value, " crossID ", crossID)
			} else {
				fmt.Println("@@@@@@@@@@@@@@", crossID)
			}
			break
		}
	}
	if !find {
		fmt.Println("not find cross ID")
	}
}
