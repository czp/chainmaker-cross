/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package sdk

import (
	"encoding/json"
	"fmt"

	"chainmaker.org/chainmaker-cross/event"
)

// UITPCrossEvent uitp cross event
type UITPCrossEvent struct {
	event *event.CrossEvent
}

// NewUITPCrossEvent Create Default CrossEvent
func NewUITPCrossEvent() *UITPCrossEvent {
	return &UITPCrossEvent{
		event.NewEmptyCrossEvent(),
	}
}

// UITPCrossSearchEvent is event for uitp cross search
type UITPCrossSearchEvent struct {
	event *event.CrossSearchEvent
}

// NewUITPCrossSearchEvent Create Default CrossEvent
func NewUITPCrossSearchEvent(crossID string) *UITPCrossSearchEvent {
	return &UITPCrossSearchEvent{
		&event.CrossSearchEvent{
			CrossID: crossID,
		},
	}
}

// UITPCrossEventResp is response for uitp cross event
type UITPCrossEventResp struct {
	CrossID string
}

// DefaultUITPCrossEventResp create new default UITPCrossEventResp
func DefaultUITPCrossEventResp() *UITPCrossEventResp {
	return &UITPCrossEventResp{}
}

// UITPCrossSearchEventResp is response for uitp cross search event
type UITPCrossSearchEventResp struct {
	*event.CrossResponse
}

// DefaultUITPCrossSearchEventResp create new default UITPCrossSearchEventResp
func DefaultUITPCrossSearchEventResp() *UITPCrossSearchEventResp {
	return &UITPCrossSearchEventResp{event.DefaultCrossResponse()}
}

// GetCrossID return crossID
func (e *UITPCrossEvent) GetCrossID() string {
	return e.event.CrossID
}

// WithMultiTxs pack CrossTx into CrossEvent
func (e *UITPCrossEvent) WithMultiTxs(txs ...*event.CrossTx) error {
	if len(e.event.TxEvents.Events) == 2 {
		return fmt.Errorf("too much txs, only support two chains")
	}
	e.event.TxEvents.Events = append(e.event.TxEvents.Events, txs...)
	return nil
}

// GetEvent return event
func (e *UITPCrossEvent) GetEvent() *event.CrossEvent {
	return e.event
}

// DeliverEvent deliver event to proxy
func (e *UITPCrossEvent) DeliverEvent(url string) (*UITPCrossEventResp, error) {
	respStr, err := Post(url+"/cross?method=InvokeCrossEvent", e.event, "application/json")
	if err != nil {
		return nil, err
	}
	resp := DefaultUITPCrossEventResp()
	err = json.Unmarshal([]byte(respStr), resp)
	return resp, err
}

// ShowCrossResult get cross result
func (e *UITPCrossEvent) ShowCrossResult(url string) (*UITPCrossSearchEventResp, error) {
	SearchEvent := NewUITPCrossSearchEvent(e.GetCrossID())
	respStr, err := Post(url+"/cross?method=GetCrossEvent", SearchEvent.event, "application/json")
	if err != nil {
		return nil, err
	}
	resp := DefaultUITPCrossSearchEventResp()
	err = json.Unmarshal([]byte(respStr), resp)
	return resp, err
}

// ShowCrossResult show cross result
func ShowCrossResult(url string, crossID string) (*UITPCrossSearchEventResp, error) {
	SearchEvent := NewUITPCrossSearchEvent(crossID)
	respStr, err := Post(url+"/cross?method=GetCrossEvent", SearchEvent.event, "application/json")
	if err != nil {
		return nil, err
	}
	resp := DefaultUITPCrossSearchEventResp()
	err = json.Unmarshal([]byte(respStr), resp)
	return resp, err
}
