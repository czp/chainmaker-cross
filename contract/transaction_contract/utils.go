/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package main

const (
	KeyCrossID      = "crossID"
	KeyExecuteData  = "executeData"
	KeyRollbackData = "rollbackData"

	KeyContractName = "contractName"
	KeyMethod       = "method"
	KeyParams       = "params"

	EmptyCrossID = ""
)

// UnpackUploadParams - check and parse transaction called params
// transaction contract call params format:
// map[crossID] 		= "crossID"
// map[executeData] 	= "executeData" 	// json string
// map[rollbackData] 	= "rollbackData" 	// json string
func UnpackUploadParams(args []*EasyCodecItem) (string, *CallContractParams, *CallContractParams) {
	var crossID = EmptyCrossID
	var eParams, rParams *CallContractParams
	// get crossID
	if v, ok := GetValueFromItems(args, KeyCrossID, EasyKeyType_USER); ok {
		crossID, _ = v.(string)
	}
	// check and parse data
	// parse eParams
	if v, ok := GetValueFromItems(args, KeyExecuteData, EasyKeyType_USER); ok {
		if eParamsBytes, convertOK := v.(string); convertOK {
			eParamsItems := EasyUnmarshal([]byte(eParamsBytes))
			eMap := EasyCodecItemToParamsMap(eParamsItems)
			eParams = callParamsFromMap(eMap)
		}
	}
	// parse eParams
	if v, ok := GetValueFromItems(args, KeyRollbackData, EasyKeyType_USER); ok {
		if rParamsBytes, convertOK := v.(string); convertOK {
			rParamsItems := EasyUnmarshal([]byte(rParamsBytes))
			rMap := EasyCodecItemToParamsMap(rParamsItems)
			rParams = callParamsFromMap(rMap)
		}
	}
	return crossID, eParams, rParams
}
