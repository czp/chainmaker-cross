/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"encoding/hex"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestMarshalUnmarshalNestMap(t *testing.T) {
	// set hex string
	setHexString := "0100000001000000010000006b010000001600000001000000010000000100000076010000000100000031"
	// sub map
	params := make(map[string]string)
	params["v"] = "1"
	// marshal sub map
	cdcs := ParamsMapToEasyCodecItem(params)
	paramsBytes := EasyMarshal(cdcs)
	// nest map
	Params := make(map[string]string)
	Params["k"] = string(paramsBytes)
	// marshal nest map
	cdcParams := ParamsMapToEasyCodecItem(Params)
	ParamsBytes := EasyMarshal(cdcParams)
	hexStr := hex.EncodeToString(ParamsBytes)
	require.Equal(t, hexStr, setHexString)
}

func TestUnmarshallParams(t *testing.T) {
	// sub params
	p := map[string]string{"v": "1"}
	cdcs := ParamsMapToEasyCodecItem(p)
	paramsBytes := EasyMarshal(cdcs)
	// pack executeParams
	executeParams := map[string]string{
		"contractName": "balance",
		"method":       "Plus",
		"params":       string(paramsBytes),
	}
	executeData := EasyMarshal(ParamsMapToEasyCodecItem(executeParams))
	// pack rollbackParams
	rollbackParams := map[string]string{
		"contractName": "balance",
		"method":       "Reset",
		"params":       "",
	}
	rollbackData := EasyMarshal(ParamsMapToEasyCodecItem(rollbackParams))
	// create params
	params := map[string]string{
		"crossID":      "test1",
		"executeData":  string(executeData),
		"rollbackData": string(rollbackData),
	}
	crossID, x, y := UnpackUploadParams(ParamsMapToEasyCodecItem(params))
	require.NotEmpty(t, crossID)
	require.NotEmpty(t, x)
	require.NotEmpty(t, y)
}
