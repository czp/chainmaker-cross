``` Test adapter
=== RUN   TestInitAdapters
--- PASS: TestInitAdapters (0.00s)
PASS
coverage: 41.9% of statements
ok      chainmaker.org/chainmaker-cross/adapter 1.599s
```

``` Test channel
=== RUN   TestInnerChannel
--- PASS: TestInnerChannel (0.00s)
=== RUN   TestNetChannel
2021-05-14 17:11:42.003 [INFO]  [NET]   net_libp2p/node.go:128  channel start listen /ip4/127.0.0.1/tcp/12345/p2p/QmSVuE22fYPeTYg4iUJJbVmwtaumYavSApR3PSm4Gb6upV

    net_channel_test.go:67: local libp2p peer id:  QmSVuE22fYPeTYg4iUJJbVmwtaumYavSApR3PSm4Gb6upV
2021-05-14 17:11:42.221 [INFO]  [NET]   channel/net_channel.go:119      cross[]->chain[]->key[test] begin write to net channel, length = [124]
2021-05-14 17:11:42.221 [INFO]  [NET]   channel/net_channel.go:128      cross[]->chain[]->key[test] write to net channel success
--- PASS: TestNetChannel (0.23s)
PASS
coverage: 34.0% of statements
ok      chainmaker.org/chainmaker-cross/channel 0.590s
```

```Test conf
=== RUN   TestInitLocalConfig
--- PASS: TestInitLocalConfig (0.00s)
=== RUN   TestStringToByte
--- PASS: TestStringToByte (0.00s)
=== RUN   TestBasePath
../config/adapters/chainmaker_sdk.yml
--- PASS: TestBasePath (0.00s)
=== RUN   TestILog
2021-05-14 17:12:59.964 [INFO]  [Test]  conf/config_test.go:68  this is INFO
2021-05-14 17:12:59.964 [INFO]  [Test]  conf/config_test.go:69  this is INFOF -> ChainCross
2021-05-14 17:12:59.964 [ERROR] [Test]  conf/config_test.go:70  this is ERROR
2021-05-14 17:12:59.964 [ERROR] [Test]  conf/config_test.go:71  this is new ERROR
2021-05-14 17:12:59.964 [ERROR] [Test]  conf/config_test.go:72  this is new new ERROR, ChainCross
2021-05-14 17:12:59.964 [ERROR] [Test]  conf/config_test.go:73  this is ERRORF -> ChainCross
--- PASS: TestILog (0.00s)
PASS
coverage: 16.3% of statements
ok      chainmaker.org/chainmaker-cross/conf    0.800s
```

```Test event
=== RUN   TestCrossEvent
--- PASS: TestCrossEvent (0.00s)
PASS
coverage: 13.5% of statements
ok      chainmaker.org/chainmaker-cross/event   0.898s
```

```Test handler
=== RUN   TestChainCallHandler
--- PASS: TestChainCallHandler (0.00s)
=== RUN   TestCrossProcessHandler
--- PASS: TestCrossProcessHandler (0.00s)
=== RUN   TestCrossSearchHandler
--- PASS: TestCrossSearchHandler (0.00s)
=== RUN   TestInitEventHandler
--- PASS: TestInitEventHandler (0.00s)
=== RUN   TestEventHandlerTools
--- PASS: TestEventHandlerTools (0.00s)
=== RUN   TestTransactionProcessHandler
--- PASS: TestTransactionProcessHandler (0.00s)
PASS
coverage: 41.7% of statements
ok      chainmaker.org/chainmaker-cross/handler 0.365s
```

```Test listener
=== RUN   TestListenerManger
2021-05-14 17:15:24.166 [INFO]  [NET]   net_libp2p/node.go:128  channel start listen /ip4/192.168.12.53/tcp/19527/p2p/QmSVuE22fYPeTYg4iUJJbVmwtaumYavSApR3PSm4Gb6upV

2021-05-14 17:15:24.170 [INFO]  [NET]   net_libp2p/node.go:128  channel start listen /ip4/192.168.12.53/tcp/19527/p2p/QmSVuE22fYPeTYg4iUJJbVmwtaumYavSApR3PSm4Gb6upV

2021-05-14 17:15:24.170 [INFO]  [CHANNEL_LISTENER]      channel_listener/channel_listener.go:153        Module channel-listener stopped
2021-05-14 17:15:24.170 [INFO]  [WEB_LISTENER]  web_listener/web_listener.go:72 Module web-listener stopped
2021-05-14 17:15:24.170 [INFO]  [CHANNEL_LISTENER]      channel_listener/channel_listener.go:153        Module channel-listener stopped
2021-05-14 17:15:24.170 [ERROR] [WEB_LISTENER]  web_listener/web_listener.go:48 Web Server Listen:listen tcp 120.0.0.1:8080: bind: can't assign requested address
2021-05-14 17:15:24.171 [INFO]  [WEB_LISTENER]  web_listener/web_listener.go:72 Module web-listener stopped
2021-05-14 17:15:24.171 [WARN]  [INNER_LISTENER]        inner_listener/inner_listener.go:65     Module inner-listener stopped
2021-05-14 17:15:24.171 [ERROR] [WEB_LISTENER]  web_listener/web_listener.go:48 Web Server Listen:listen tcp 120.0.0.1:8080: bind: can't assign requested address
2021-05-14 17:15:24.171 [WARN]  [INNER_LISTENER]        inner_listener/inner_listener.go:65     Module inner-listener stopped
--- PASS: TestListenerManger (0.01s)
PASS
coverage: 83.3% of statements
ok      chainmaker.org/chainmaker-cross/listener        0.654s
```

```Test logger
=== RUN   TestLogger
--- PASS: TestLogger (0.00s)
PASS
coverage: 71.2% of statements
ok      chainmaker.org/chainmaker-cross/logger  0.331s
```

```Test net
--- PASS: TestHost (0.00s)
=== RUN   TestHostConnectionInteractions
--- PASS: TestHostConnectionInteractions (0.00s)
=== RUN   TestPrivateKeyFromPEM
--- PASS: TestPrivateKeyFromPEM (0.00s)
=== RUN   TestPrivateKeyFromPEMFilePath
--- PASS: TestPrivateKeyFromPEMFilePath (0.00s)
PASS
coverage: 56.2% of statements
ok      chainmaker.org/chainmaker-cross/net/net_libp2p  2.010s
```

```Test prover
=== RUN   TestProver
--- PASS: TestProver (0.00s)
PASS
coverage: 74.1% of statements
ok      chainmaker.org/chainmaker-cross/prover  0.350s
```

```Test store
=== RUN   TestKvStateDB_StartAndFinishCross
2021-05-14 17:17:40.918 [WARN]  [STORAGE]       kvdb/state_kvdb.go:105  can not find value for key[CR/910437000]
2021-05-14 17:17:40.927 [INFO]  [STORAGE]       leveldb/leveldb_provider.go:124 Module storage stopped
--- PASS: TestKvStateDB_StartAndFinishCross (0.05s)
=== RUN   TestKvStateDB_ChainIDs
2021-05-14 17:17:40.987 [INFO]  [STORAGE]       leveldb/leveldb_provider.go:124 Module storage stopped
--- PASS: TestKvStateDB_ChainIDs (0.06s)
PASS
coverage: 72.9% of statements
ok      chainmaker.org/chainmaker-cross/store/kvdb      0.451s
```

```Test utils
=== RUN   TestBase64EncodeToString
--- PASS: TestBase64EncodeToString (0.00s)
=== RUN   TestMyChannel
-----zhangsan
--- PASS: TestMyChannel (1.00s)
=== RUN   TestNewUUID
--- PASS: TestNewUUID (0.00s)
=== RUN   TestRandomBySeed
--- PASS: TestRandomBySeed (0.00s)
=== RUN   TestChannel
zhangsan
--- PASS: TestChannel (10.00s)
PASS
coverage: 83.3% of statements
ok      chainmaker.org/chainmaker-cross/utils   11.304s
```